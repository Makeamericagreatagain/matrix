(ns repltrader.execution
  (:require [repltrader.gateway :as gateway]

            [matrix.indicators :as indicators])
  (:use clojure.pprint)
  (:gen-class))

;(gateway/connect)
;(gateway/subscribe prn)


(defn BUY [tic quantity]
  (gateway/place-order {:symbol tic :type :equity :exchange "SMART" :currency "USD"}
                       {:transmit? true :action :buy :type :market :quantity quantity
                        :outside-regular-trading-hours? false}))
(defn buy-hedge-new
  "tic is a keyword here, need to resolve this"
  [tic self-qty spy-qty]
  (when (neg? self-qty) (println "Error - negative sizes..."))
  (when (and  (not (Double/isNaN self-qty))  (not (zero? self-qty))) (gateway/place-order {:symbol (name tic) :type :equity :exchange "SMART" :currency "USD"}
                                                                                          {:transmit? true :action :buy :type :market :quantity self-qty
                                                                                           :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel})
        (gateway/place-order {:symbol "SPY" :type :equity :exchange "SMART" :currency "USD"}
                             {:transmit? true :action :sell :type :market :quantity spy-qty
                              :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel})))

(defn sell-hedge-new
  "tic is a keyword here, need to resolve this"
  [tic self-qty spy-qty]
  (when (neg? self-qty) (println "Error - negative sizes..."))
  (when (and  (not (Double/isNaN self-qty))  (not (zero? self-qty))) (gateway/place-order {:symbol (name tic) :type :equity :exchange "SMART" :currency "USD"}
                                                                                          {:transmit? true :action :sell :type :market :quantity self-qty
                                                                                           :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel})
        (gateway/place-order {:symbol "SPY" :type :equity :exchange "SMART" :currency "USD"}
                             {:transmit? true :action :buy :type :market :quantity spy-qty
                              :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel})))

(defn sell-hedge
  "tic is a keyword here, need to resolve this"
  [capital indicator-state tic date]
  (let [p (:data (indicators/position-to-take capital indicator-state tic date))
        p-market (:data (indicators/position-to-take capital indicator-state :SPY date))]

    (when (and  (not (Double/isNaN p))  (not (zero? p)))

      (gateway/place-order {:symbol (name tic) :type :equity :exchange "SMART" :currency "USD"}
                           {:transmit? true :action :sell :type :market :quantity p
                            :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel}) (gateway/place-order {:symbol "SPY" :type :equity :exchange "SMART" :currency "USD"}
                                                                                                                             {:transmit? true :action :buy :type :market :quantity p-market
                                                                                                                              :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel})))) (defn buy-hedge
                                                                                                                                                                                                               "tic is a keyword here, need to resolve this"
                                                                                                                                                                                                               [capital indicator-state tic date]
                                                                                                                                                                                                               (let [p (:data (indicators/position-to-take capital indicator-state tic date))
                                                                                                                                                                                                                     p-market (:data (indicators/position-to-take capital indicator-state :SPY date))]

                                                                                                                                                                                                                 (when (and  (not (Double/isNaN p))  (not (zero? p)))

                                                                                                                                                                                                                   (gateway/place-order {:symbol (name tic) :type :equity :exchange "SMART" :currency "USD"}
                                                                                                                                                                                                                                        {:transmit? true :action :buy :type :market :quantity p
                                                                                                                                                                                                                                         :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel}) (gateway/place-order {:symbol "SPY" :type :equity :exchange "SMART" :currency "USD"}
                                                                                                                                                                                                                                                                                                                                          {:transmit? true :action :sell :type :market :quantity p-market
                                                                                                                                                                                                                                                                                                                                           :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel})))) (defn BUY-LIMIT-IOC [tic price quantity] (gateway/place-order {:symbol tic :type :equity :exchange "SMART" :currency "USD"}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {:transmit? true :action :buy :type :limit :limit-price price :quantity quantity
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel}))

(defn SELL-LIMIT-IOC [tic price quantity] (gateway/place-order {:symbol tic :type :equity :exchange "SMART" :currency "USD"}
                                                               {:transmit? true :action :sell :type :limit :limit-price price :quantity quantity
                                                                :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel}))

(defn BUY-LIMIT [tic price quantity] (gateway/place-order {:symbol tic :type :equity :exchange "SMART" :currency "USD"}
                                                          {:transmit? true :action :buy :type :limit :limit-price price :quantity quantity
                                                           :outside-regular-trading-hours? false}))

(defn SELL-LIMIT [tic price quantity] (gateway/place-order {:symbol tic :type :equity :exchange "SMART" :currency "USD"}
                                                           {:transmit? true :action :sell :type :limit :limit-price price :quantity quantity
                                                            :outside-regular-trading-hours? false}))

(defn SELL-OLD [tic quantity]
  (gateway/place-order {:symbol tic :type :equity :exchange "SMART" :currency "USD"}
                       {:transmit? true :action :sell :type :market :quantity quantity
                        :outside-regular-trading-hours? false :time-in-force :immediate-or-cancel}))

(defn SELL [tic quantity]
  (gateway/place-order {:symbol tic :type :equity :exchange "SMART" :currency "USD"}
                       {:transmit? true :action :sell :type :market :quantity quantity
                        :outside-regular-trading-hours? false}))

;(BUY-FX "EUR.USD")
