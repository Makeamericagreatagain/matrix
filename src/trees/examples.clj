(ns trees.examples
  (:require [trees.dataframe :as df]
            [matrix.utils :as utils]))

(def sample-data
  [[:age :married? :own-house? :income :gender :class]
   [22 :no :no 28000 :male :bad]
   [46 :no :yes 32000 :female :bad]
   [24 :yes :yes 24000 :male :bad]
   [25 :no :no    27000 :male :bad]
   [29 :yes :yes  32000 :female :bad]
   [45 :yes :yes  30000 :female :good]
   [63 :yes :yes  58000 :male :good]
   [36 :yes :no   52000 :male :good]
   [23 :no :yes   40000 :female :good]
   [50 :yes :yes  28000 :female :good]])

(def rich-example {:age 50 :married? true :own-house? true :income 200000 :gender :male})
(def poor-example {:age 50 :married? true :own-house? false :income 25000 :gender :male})
(def notes-example {:age 42 :married? false :own-house? true :income 30000 :gender :male})

(defn yn-bool
  [x]
  (cond (= x :no) false
        (= x :yes) true))

(defn parse-float
  [x]
  (Float/parseFloat x))

(defn load-sample-data
  []
  (-> (df/from-tabular (first sample-data) (rest sample-data) {})
      (df/typify-attribute-df :age :int :numerical identity)
      (df/typify-attribute-df :married? :boolean :categorical yn-bool)
      (df/typify-attribute-df :own-house? :boolean :categorical yn-bool)
      (df/typify-attribute-df :income :int :numerical identity)
      (df/typify-attribute-df :gender :enum :categorical identity)
      (df/typify-attribute-df :class :enum :categorical identity)))

(defn load-iris-data
  []
  (-> (df/from-csv "resources/data/iris.tsv" \tab)
      (df/typify-attribute-df "Petal width" :float :numerical parse-float)
      (df/typify-attribute-df "Sepal width" :float :numerical parse-float)
      (df/typify-attribute-df "Petal length" :float :numerical parse-float)
      (df/typify-attribute-df "Sepal length" :float :numerical parse-float)
      (df/typify-attribute-df "Species" :enum :categorical identity)))

;(def features #{"crim" "zn" "indus" "chas" "nox" "rm" "age" "dis" "rad" "tax" "ptratio" "black" "lstat"})
;(def target #{"medv"})

(defn load-housing-data
  []
  (-> (df/from-csv "resources/data/housing.csv")
      (df/typify-attribute-df "crim" :float :numerical parse-float)
      (df/typify-attribute-df "zn" :float :numerical parse-float)
      (df/typify-attribute-df "indus" :float :numerical parse-float)
      (df/typify-attribute-df "chas" :float :numerical parse-float)
      (df/typify-attribute-df "nox" :float :numerical parse-float)
      (df/typify-attribute-df "rm" :float :numerical parse-float)
      (df/typify-attribute-df "age" :float :numerical parse-float)
      (df/typify-attribute-df "dis" :float :numerical parse-float)
      (df/typify-attribute-df "rad" :float :numerical parse-float)
      (df/typify-attribute-df "tax" :float :numerical parse-float)
      (df/typify-attribute-df "ptratio" :float :numerical parse-float)
      (df/typify-attribute-df "black" :float :numerical parse-float)
      (df/typify-attribute-df "lstat" :float :numerical parse-float)
      (df/typify-attribute-df "medv" :float :numerical parse-float)))

(defn load-trading-data
  []
  (-> (df/from-csv (str utils/HOME "/data/trading-fsg.tsv"))
      (df/typify-attribute-df "linear-regression-slope" :float :numerical read-string)
      (df/typify-attribute-df "linear-regression-slope-diff" :float :numerical read-string)

      (df/typify-attribute-df "returns-quality" :float :numerical read-string)
      (df/typify-attribute-df "dispersion" :float :numerical read-string)

      (df/typify-attribute-df "stdev-ema-20" :float :numerical read-string)

      (df/typify-attribute-df "volume-ema-20" :float :numerical read-string)

      (df/typify-attribute-df "price-ema-26" :float :numerical read-string)

      (df/typify-attribute-df "price-ema-12" :float :numerical read-string)

      (df/typify-attribute-df "price-ema-5" :float :numerical read-string) (df/typify-attribute-df "col-0" :enum :categorical identity)))

;(println (load-trading-data))
