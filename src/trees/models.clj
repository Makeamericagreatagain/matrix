(ns trees.models

  (:require
   [trees.dataframe :refer [df->maps]]
   [trees.examples :refer [load-trading-data]]
   [trees.measures :refer [accuracy]]
   [trees.sampling :refer [train-test-split]]
   [trees.sampling :refer [generate-bootstrap-sample]]
   [trees.dostuff :refer [learn classify]]
  ;[trees.regression :refer [learn predict]]

   [clojure-csv.core :as csv]
   [clojure.java.io :as io]
   [trees.common :refer :all]
   [trees.dataframe :as df]
   [trees.math :refer [sum]]
   [trees.measures :as m]
   [trees.splitting :refer [calculate-split]]
   [taoensso.timbre :as log])

;; i want aot compile on this
;(:gen-class)
)

(def iris-data (load-trading-data))

(def split (train-test-split iris-data 0.50))
(def training-set (first split))
(def test-set (df->maps (second split)))

(def features #{"linear-regression-slope" "linear-regression-slope-diff" "returns-quality" "dispersion"})
(def target "col-0")

;(def iris-tree-model1 (learn training-set features target))


;(def truth (map #(get % "col-0") test-set))
;(def predicted-model1 (map #(classify iris-tree-model1 %) test-set))

;(accuracy truth predicted-model1)
;(println truth)
;(println predicted-model1)
;; To view the tree:
;(require '[com.walmartlabs.datascope :as ds])
;(ds/view iris-tree)


;; drops after i add stdev-ema-20


;(def iris-data-bootstrap (generate-bootstrap-sample iris-data N))


;; do model1 and model2 agree?

(defn majority-vote [x y]
  (cond (= x y) x
        :else "0"))

;(def verdict (map vector truth (map majority-vote predicted-model1 predicted-model2)))
;(def clean-verdict (filter #(not (zero? (read-string (second %)))) verdict ))
;(count clean-verdict)
;(count verdict)
;(println verdict)
;(println clean-verdict)
;(comment (accuracy (map first clean-verdict ) (map second clean-verdict )))
;(println truth)
;(println predicted)
;; To view the tree:
;(require '[com.walmartlabs.datascope :as ds])
;(ds/view iris-tree)
;(class iris-tree-model1)
;(println iris-tree-model1)


;(classify iris-tree-model2 (first test-set))
;(first test-set)


(comment (classify iris-tree-model1 {"dispersion" 398.74,
                                     "pseudo-price-ema-12" 15.868369,
                                     "recent-returns" -122.09,
                                     "returns-quality" -0.23,
                                     "stdev-ema-20" 44.438774,
                                     "linear-regression-slope" 1.9553571428571441,
                                     "volume-ema-20" 3359.4915,
                                     "pseudo-price-ema-26" 21.302526,
                                     "pseudo-price-ema-5" -5.321302,
                                     "linear-regression-slope-diff" -2.0524285714285684,
                                     "price-ema-26" 76.07172,
                                     "price-ema-5" 75.86961,
                                     "returns-from-open" -56.65,
                                     "price-ema-12" 76.030464}))
